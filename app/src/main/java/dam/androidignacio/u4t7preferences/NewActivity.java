package dam.androidignacio.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class NewActivity extends AppCompatActivity {

    TextView tvPreferences;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        setUI();
    }

    private void setUI() {

        this.tvPreferences = findViewById(R.id.tvPreferences);

        bundle = getIntent().getExtras();

        tvPreferences.setText("PLAYER: " + bundle.getString("PlayerName") + System.lineSeparator() +
                "SCORE: " + bundle.getInt("Score") + System.lineSeparator() +
                "LEVEL: " + bundle.getInt("Level") + System.lineSeparator() +
                "DIFFICULTY: " + bundle.getString("Difficulty") + System.lineSeparator() +
                "SOUND: " + bundle.getBoolean("SoundActive") + System.lineSeparator() +
                "BG COLOR: " + bundle.getString("BackgroundColor"));

    }

}