package dam.androidignacio.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class U4T7Preferences extends AppCompatActivity implements View.OnClickListener {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName, etScore;
    private Spinner spinnerLevel, spinnerBackground;
    private RadioGroup rgDifficulty, rgPreferences;
    private CheckBox checkBoxSound;
    private Button btQuit, btPreferences;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u4t7preferences);

        setUI();
    }

    private void setUI() {

        this.bundle = new Bundle();

        etPlayerName = findViewById(R.id.etPlayerName);
        etScore = findViewById(R.id.etScore);

        // Level spinner, set aadapter from string-array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerLevelAdapter = ArrayAdapter.createFromResource(this, R.array.levels,
                android.R.layout.simple_spinner_item);
        spinnerLevelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerLevelAdapter);

        // Background spinner, set aadapter from string-array resource
        spinnerBackground = findViewById(R.id.spinnerBackground);
        ArrayAdapter<CharSequence> spinnerBackgroundAdapter = ArrayAdapter.createFromResource(this, R.array.colors,
                android.R.layout.simple_spinner_item);
        spinnerBackgroundAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBackground.setAdapter(spinnerBackgroundAdapter);

        checkBoxSound = findViewById(R.id.checkBoxSound);

        rgDifficulty = findViewById(R.id.rgDifficulty);
        rgPreferences = findViewById(R.id.rgPreferences);
        rgPreferences.check(R.id.rbGetSharedPreferences);

        btPreferences = findViewById(R.id.btPreferences);
        btPreferences.setOnClickListener(this);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    // save activity data to preferences file
    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences myPreferences = null;

        // get preference file
        switch (rgPreferences.getCheckedRadioButtonId()) {
            case R.id.rbGetSharedPreferences: myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE); break;
            case R.id.rbGetPreferences: myPreferences = getPreferences(MODE_PRIVATE); break;
            case R.id.rbGetDefaultSharedPreferences: myPreferences = PreferenceManager.getDefaultSharedPreferences(this); break;
        }

        // save UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putBoolean("SoundActive", checkBoxSound.isChecked());
        editor.putString("BackgroundColor", (String) spinnerBackground.getSelectedItem());
        editor.putString("Difficulty", getDifficulty());
        editor.putInt("Level", (spinnerLevel.getSelectedItemPosition() + 1));

        // save data on bundle to pass to New Activity
        bundle.putString("PlayerName", etPlayerName.getText().toString());
        bundle.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        bundle.putBoolean("SoundActive", checkBoxSound.isChecked());
        bundle.putString("BackgroundColor", (String) spinnerBackground.getSelectedItem());
        bundle.putString("Difficulty", getDifficulty());
        bundle.putInt("Level", (spinnerLevel.getSelectedItemPosition() + 1));

        editor.commit();

    }

    // read activity data from preferences file
    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences myPreferences = null;

        // get preference file
        switch (rgPreferences.getCheckedRadioButtonId()) {
            case R.id.rbGetSharedPreferences: myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE); break;
            case R.id.rbGetPreferences: myPreferences = getPreferences(MODE_PRIVATE); break;
            case R.id.rbGetDefaultSharedPreferences: myPreferences = PreferenceManager.getDefaultSharedPreferences(this); break;
        }

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        checkBoxSound.setChecked(myPreferences.getBoolean("SoundActive", false));
        spinnerBackground.setSelection(getSelectedColor(myPreferences), true);
        setDifficultyCheck(myPreferences.getString("Difficulty", "Normal"));
        spinnerLevel.setSelection((myPreferences.getInt("Level", 1) - 1), true);

    }

    private int getSelectedColor(SharedPreferences myPreferences) {

        String color = myPreferences.getString("BackgroundColor", "White");
        String[] colors = getResources().getStringArray(R.array.colors);
        String[] colorsValues = getResources().getStringArray(R.array.colorValues);

        for (int i = 0; i < colors.length; i++) {
            if (colors[i].equals(color)) {
                View view = this.getWindow().getDecorView();
                view.setBackgroundColor(Color.parseColor(colorsValues[i]));
                return i;
            }
        }
        return 0;

    }

    private String getDifficulty() {
        switch (rgDifficulty.getCheckedRadioButtonId()) {
            case R.id.rbEasy: return getString(R.string.rbEasy);
            case R.id.rbNormal: return getString(R.string.rbNormal);
            case R.id.rbHard: return getString(R.string.rbHard);
            case R.id.rbVeryHard: return getString(R.string.rbVeryHard);
            default: return getString(R.string.rbExpert);
        }
    }

    private void setDifficultyCheck(String difficulty) {
        switch (difficulty) {
            case "Easy": rgDifficulty.check(R.id.rbEasy); break;
            case "Normal": rgDifficulty.check(R.id.rbNormal); break;
            case "Hard": rgDifficulty.check(R.id.rbHard); break;
            case "Very Hard": rgDifficulty.check(R.id.rbVeryHard); break;
            case "Expert": rgDifficulty.check(R.id.rbExpert); break;
        }
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, NewActivity.class).putExtras(bundle));
    }
}